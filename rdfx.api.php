<?php
// $Id: votingapi.api.php,v 1.1.2.1.2.1 2009/07/01 07:26:12 eaton Exp $

/**
 * @file
 * Provides hook documentation for the RDFx module.
 */

function hook_rdfx_term_types_alter(){}


/**
 * @param $uri
 *  Aliased url to Resource
 * @param $context
 *  Context for resource uri, contains :
 *  - entity : resource entity
 *  - uri : resource uri
 */
function hook_rdfx_get_rdf_model_alter(&$index,$context){
}

/**
 * @param $uri
 *  Aliased url to Resource
 * @param $context
 *  Context for resource uri, contains :
 *  - resource_id : ID of the resource,  node id (nid) , taxonomy term id (tid) ...
 *  - resource_type :  possible values are 'node' , 'taxonomy_term' , ...
 */
function hook_rdfx_resource_uri_alter(&$uri,$context){
}

/**
 * @param $object_uri
 *  Aliased url to Object
 * @param array $context
 *  Context for object, contains :
 *  - resource_id : ID of the resource,  node id (nid) , taxonomy term id (tid) ...
 *  - resource_type :  possible values are 'node' , 'taxonomy_term' , ...
 *  - predicates : rdf mapping info
 */
function hook_rdfx_add_resource_alter(&$object_uri,$context){
}

function hook_rdfx_add_literal_alter(&$object_value,$predicates){
}
